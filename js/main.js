const hashcode = require('./hashcode');
const optimize = require('./optimize');

const input = hashcode.loadFile(process.argv[2]);
console.log(input);
const output = optimize(input);
console.log(output);
hashcode.saveFile(hashcode.streamResult(output), `${process.argv[2].slice(0, -2)}out`);
