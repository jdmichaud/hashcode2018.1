const hashcode = require('./hashcode');
const optimize = require('./optimize-jd');

const input = hashcode.loadFile(process.argv[2]);
console.log('input:', input);

const results = optimize.optimize(input);
const streamableResults = results.map((result) =>
  result.ride.filter(r => r.id !== undefined).map(r => r.id)
);
console.log(streamableResults);
console.log(results.map(r => r.score).reduce((acc, value) => acc + value, 0));
hashcode.saveFile(hashcode.streamResult(streamableResults), `${process.argv[2].slice(0, -2)}out`);
