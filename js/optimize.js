const lodash = require('lodash');

const constants = require('./constants');

function getAvailableRides(rides, vPos, currentStep, nbstep) {
  return rides
    .filter(ride => isRidePossible(ride, vPos, currentStep, nbstep))
    .sort((a, b) => {
      const startStepA = getStartStep(a, vPos, currentStep);
      const startStepB = getStartStep(b, vPos, currentStep);

      if (startStepA === startStepB) {
        return 0;
      }

      return startStepA < startStepB ? -1 : 1;
    });
}

function getDistance(a, b) {
  return Math.abs(a[0] - b[0]) + Math.abs(a[1] - b[1]);
}

function getStartStep(ride, vPos, currentStep) {
  const { startpos, startdate } = ride;
  const distToStart = getDistance(vPos, startpos);
  return currentStep + distToStart +
    Math.max(startdate - (currentStep + distToStart), 0)
}

function isRidePossible(ride, vPos, currentStep, nbstep) {
  const { startpos, finishpos, finishdate } = ride;
  const rideDist = getDistance(startpos, finishpos);
  const startStep = getStartStep(ride, vPos, currentStep);
  const finishStep = startStep + rideDist;

  return finishStep <= finishdate && finishStep <= nbstep;
}

function optimize(input) {
  const { parameters, rides } = input;
  const { nbrow, nbcol, nbcar, nbride, bonus, nbstep } = parameters;
  const keepRides = rides.slice();
  const res = [];

  for (let i = 0; i < nbcar; i++) {
    let currentStep = 0;
    let vPos = [0, 0];
    const carRes = [];

    let availableRides = getAvailableRides(keepRides, vPos, currentStep, nbstep);

    while (availableRides.length > 0) {
      const ride = availableRides[0];
      const { startpos, finishpos } = ride;

      carRes.push(rides.indexOf(ride));
      keepRides.splice(keepRides.indexOf(ride), 1);

      const rideDist = getDistance(startpos, finishpos);
      const startStep = getStartStep(ride, vPos, currentStep);

      currentStep = startStep + rideDist;
      vPos = finishpos;

      availableRides = getAvailableRides(keepRides, vPos, currentStep, nbstep);
    }

    res.push(carRes);
  }

  return res;
}

module.exports = optimize;
