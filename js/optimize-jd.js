const lodash = require('lodash');

// Distance of a ride
function ridedist(ride) {
  return Math.abs(ride.startpos[0] - ride.finishpos[0]) +
    Math.abs(ride.startpos[1] - ride.finishpos[1])
}

// Distance between two rides
function dist(lhs, rhs) {
  return Math.abs(lhs.finishpos[0] - rhs.startpos[0]) +
    Math.abs(lhs.finishpos[1] - rhs.startpos[1])
}

// How much step the cat made since start
function stepForCar(car) {
  const distances = car.map((ride, index) => {
    const rideDist = ridedist(ride);
    let nextDist = 0;
    if (index < car.length) {
      nextDist = ridedist(ride, car[index + 1]);
    }
    return rideDist + nextDist;
  });
  return distances.reduce((acc, value) => acc + value, 0);
}

function score(car) {
  let i = 0;
  let step = 0;
  let score = 0;
  while (i < car.length) {
    for (; i < car.length && car[i].id === undefined; ++i) {
      step += ridedist(car[i]);
    }
    if (i === car.length) break;
    for (; i < car.length && car[i].id !== undefined; ++i) {
      if (car[i].startdate === step) score += this.game.parameters.bonus;
      if (car[i].finishdate >= step + ridedist(car[i]))
        score += car[i].finishdate - (step + ridedist(car[i]));
        step += ridedist(car[i]);
    }
    if (i === car.length) break;
  }
  return score;
}

// stepForCar([{ startpos: [0, 0], finishpos: [0, 0] }]);

function nextRide(car, rides) {
  let firstpos;
  if (car.lenght > 0) {
    firstpos = car[car.length - 1];
  } else {
    firstpos = { startpos: [0, 0], finishpos: [0, 0] };
  }
  const found = rides.find((candidate) => {
    return ((stepForCar(car) + dist(firstpos, candidate) + ridedist(candidate)) <= candidate.finishdate);
  });
  return found;
}

function knapsack(car, rides, maxscore = 0) {
  let bestride = [];
  if (rides.length === 0) {
    return {
      score: score(car),
      ride: car,
    }
  }
  rides.forEach((next, i) => {
    // Let's get to the next ride
    const nextSteps = [];
    if (car.length === 0) {
      nextSteps.push({ startpos: [0, 0], finishpos: next.startpos });
    } else {
      nextSteps.push({ startpos: car[car.length - 1].finishpos, finishpos: next.startpos })
    }
    nextSteps.push(next);
    const m = score(car.concat(nextSteps));
    if (m > maxscore) {
      console.log('maxscore', maxscore);
      const result = knapsack(car.concat(nextSteps), rides.filter(r => r.id !== next.id), maxscore);
      // console.log('ride', result.ride.filter(r => r.id !== undefined).map(r => r.id), 'score', result.score);
      if (result.score >= maxscore &&
        (bestride.length === 0 || result.ride.length < bestride.length)) {
        maxscore = result.score;
        bestride = result.ride;
      }
    }
  });
  const s = score(car);
  if (s >= maxscore &&
    (bestride.length === 0 || car.length < bestride.length)) {
    maxscore = s;
    bestride = car;
  }
  return {
    score: maxscore,
    ride: bestride,
  }
}

function optimize(game) {
  this.games = game;
  const rides = game.rides.sort();
  results = [];
  for (let car = game.parameters.nbcar - 1; car >= 0; --car) {
    console.log(car);
    const takenRides = lodash.flatten(results.map(r => r.ride));
    results.push(knapsack([], lodash.difference(game.rides, takenRides)));
  }
  return results;
}

module.exports = {
  optimize,
};
