const fs = require('fs');
const iconv = require('iconv-lite');
const lodash = require('lodash');

const constants = require('./constants');

/**
 * Convert from the original latin1 to proper utf8
 * @param buffer {Buffer} Take a javascript Buffer object as returned by readfile
 * @returns a utf-8 encoded string
 */
function fixEncoding(buffer) {
  // Convert from an encoded buffer to js string.
  return iconv.decode(buffer, 'latin1');
}

function decodeParameters(line) {
  const values = line.split(/\W+/);
  return {
    nbrow: parseInt(values[0], 10),
    nbcol: parseInt(values[1], 10),
    nbcar: parseInt(values[2], 10),
    nbride: parseInt(values[3], 10),
    bonus: parseInt(values[4], 10),
    nbstep: parseInt(values[5], 10),
  };
}

function loadFile(filepath) {
  const lines = fixEncoding(fs.readFileSync(filepath)).split('\n');
  const parameters = decodeParameters(lines[0]);
  // Load rest of file
  const rides = [];
  for (let i = 1; i < parameters.nbride + 1; ++i) {
    const values = lines[i].split(/\W+/);
    const ride = {
      id: i - 1,
      startpos: [ parseInt(values[0], 10), parseInt(values[1], 10) ],
      finishpos: [ parseInt(values[2], 10), parseInt(values[3], 10) ],
      startdate: parseInt(values[4], 10),
      finishdate: parseInt(values[5], 10),
    };
    rides.push(ride);
  }
  return {
    parameters,
    rides,
  };
}

function streamResult(results) {
  stream = '';
  results.forEach(rides => {
    stream += `${rides.length} ${rides.join(' ')}\n`;
  })
  return stream;
}

function saveFile(results, filepath) {
  fs.writeFileSync(filepath, results);
}

module.exports = {
  loadFile,
  saveFile,
  streamResult,
};
